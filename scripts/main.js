'use_strict';
/**
 * The script for generating and manipulating the spending log
 * @author Mykyta Onipchenko
 * @author Bogdan Ivan
 */


let spendingArray;
document.addEventListener('DOMContentLoaded', setup);



/**
 * Initialize the form by adding a event listener on submission
 * that adds spending entries on the page 
 */
function setup() {
    const form = document.querySelector('#spendingLogForm');
    form.addEventListener('submit', submit);

    spendingArray = loadSpendings();
    refreshAllSpendings();
}


/**
 * Form submission callback
 * Add the spending entry to the site and to the array
 * @param {Event} e Event object
 */
function submit(e) {
    // Prevent page reloading when the form is submitted
    e.preventDefault();

    // Get the data from the form
    const in_date = document.querySelector('#in_date').value;
    const in_desc = document.querySelector('#in_desc').value;
    const in_amount = document.querySelector('#in_amount').value;
    const in_category = document.querySelector('input[name=in_category]:checked').value;

    // Generate the JSON object with the form data
    const new_info = {
        date: in_date,
        desc: in_desc,
        amount: in_amount,
        category: in_category,
        id: Date.now()
    }

    // Clear the form
    document.querySelector('#in_desc').value = '';
    document.querySelector('#in_amount').value = '';

    // Update internal arrays
    spendingArray.push(new_info);
    updateLocalStorage();
    addNewEntryToTheDom(new_info);

}

/**
 * Add an article representing an spending to the DOM
 * @param {Object} info The information about the spending (date, description, amount, category)
 */
function addNewEntryToTheDom(info) {
    // Search section to put the article inside
    const entries = document.querySelector('#entries');

    // Create the article
    const spendingArticle = document.createElement('article');
    spendingArticle.classList = info.category;
    spendingArticle.setAttribute('dateAdded', info.id);
    // Delete the article when the button inside is pressed
    spendingArticle.addEventListener('click', checkedAction);

    //#region Text data section
    // Section
    const entryTextData = document.createElement('section');
    entryTextData.classList = 'entryTextData';
    // Description
    const descParagraph = document.createElement('h2');
    descParagraph.textContent = info.desc;
    descParagraph.classList = 'entryDescription';
    // Category
    const categoryParagraph = document.createElement('p');
    categoryParagraph.textContent = info.category;
    categoryParagraph.classList = 'entryCategory';
    // Amount
    const amountParagraph = document.createElement('p');
    amountParagraph.textContent = info.amount;
    amountParagraph.classList = 'entryAmountSpent';
    // Date
    const dateParagraph = document.createElement('p');
    dateParagraph.textContent = info.date;
    dateParagraph.classList = 'entryDate';
    // Append
    entryTextData.appendChild(descParagraph);
    entryTextData.appendChild(categoryParagraph);
    entryTextData.appendChild(amountParagraph);
    entryTextData.appendChild(dateParagraph);
    //#endregion

    //#region Other data section
    // Section
    const entryOtherData = document.createElement('section');
    entryOtherData.classList = 'entryOtherData';
    // Button
    const btn = document.createElement('button');
    btn.textContent = 'X'
    btn.classList = 'entryDelete';
    // Stars
    const starParagraph = document.createElement('p');
    starParagraph.classList = 'entryStars'
    // Calculate the number of stars based on the "Amount Spent" input
    if(info.amount <= 100) {
        starParagraph.textContent = '';
    }
    else if (info.amount <= 500) {
        starParagraph.textContent = '\u2605';
    }
    else if (info.amount <= 750) {
        starParagraph.textContent = '\u2605\u2605';
    }
    else {
        starParagraph.textContent = '\u2605\u2605\u2605';
    }
    // Append
    entryOtherData.appendChild(btn);
    entryOtherData.appendChild(starParagraph);
    //#endregion

    // Finalize article
    spendingArticle.appendChild(entryTextData);
    spendingArticle.appendChild(entryOtherData);
    entries.appendChild(spendingArticle);

}

/**
 * Remove the entry from the DOM and from the array
 * @param {Event} e Event object
 */
function checkedAction(e) {
    // Do nothing if anything but the button is pressed
    if (e.target.tagName != 'BUTTON') return;

    // Find the index of the current spending
    const index = spendingArray.findIndex((elem) => { return elem.id == e.currentTarget.getAttribute('dateAdded') });

    // Remove the spending from the spendingArray
    if (index > -1) {
        spendingArray.splice(index, 1);
    }
    updateLocalStorage();

    // Rebuild the spending article list
    refreshAllSpendings();
}


/**
 * Clear the spending list and rebuild it from scratch
 */
function refreshAllSpendings() {
    // Clear the section that contains all the spendings
    document.querySelector('#entries').textContent = '';

    // Rebuild the spending list from scratch
    spendingArray.forEach(
        (spending) => {
            addNewEntryToTheDom(spending)
        }
    );
}


/**
 * Load the spending array from local storage if exists
 * @returns {Object[]} Array of spendings or empty array if no local storage data
 */
function loadSpendings() {
    if (localStorage.getItem('spendings')) {
        // Data exists, load it
        return JSON.parse(localStorage.getItem('spendings'));
    }
    else {
        // First load, init to empty
        return [];
    }
}


/**
 * Overwrite the local storage to be the current value of the spending array
 */
function updateLocalStorage() {
    localStorage.setItem('spendings', JSON.stringify(spendingArray));
}
